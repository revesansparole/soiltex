========================
soiltex
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/soiltex/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/soiltex/

.. image:: https://revesansparole.gitlab.io/soiltex/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/soiltex/1.0.0/

.. image:: https://revesansparole.gitlab.io/soiltex/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/soiltex

.. image:: https://badge.fury.io/py/soiltex.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/soiltex

.. #}
.. {# pkglts, glabpkg, after doc

develop: |dvlp_build|_ |dvlp_coverage|_

.. |dvlp_build| image:: https://gitlab.com/revesansparole/soiltex/badges/develop/pipeline.svg
.. _dvlp_build: https://gitlab.com/revesansparole/soiltex/commits/develop

.. |dvlp_coverage| image:: https://gitlab.com/revesansparole/soiltex/badges/develop/coverage.svg
.. _dvlp_coverage: https://gitlab.com/revesansparole/soiltex/commits/develop


master: |master_build|_ |master_coverage|_

.. |master_build| image:: https://gitlab.com/revesansparole/soiltex/badges/master/pipeline.svg
.. _master_build: https://gitlab.com/revesansparole/soiltex/commits/master

.. |master_coverage| image:: https://gitlab.com/revesansparole/soiltex/badges/master/coverage.svg
.. _master_coverage: https://gitlab.com/revesansparole/soiltex/commits/master
.. #}

Soil texture definition


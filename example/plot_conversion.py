"""
Texture triangle
================

Associate name of texture to zone in clay, sand, silt triangle.
"""
from math import cos, radians

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from soiltex.conversion import texture_name

# compute texture triangle
records = []
for clay in np.linspace(0, 1, 100):
    for sand in np.linspace(0, 1, 100):
        if clay + sand <= 1:
            name = texture_name(clay, sand)
            records.append(dict(
                name=name,
                clay=clay * 100,
                sand=sand * 100
            ))

df = pd.DataFrame(records)

# convert tex coords to cartesian triangle
df['x'] = 100 - df['sand'] - df['clay'] * cos(radians(60))
df['y'] = df['clay'] * cos(radians(30))

# plot texture triangle
cmap = cm.get_cmap('tab20')

fig, axes = plt.subplots(1, 1, figsize=(8, 8), squeeze=False)

ax = axes[0, 0]
for i, (name, sdf) in enumerate(df.groupby(by='name')):
    ax.plot(sdf['x'], sdf['y'], 'o', color=cmap(i), markersize=4)
    ax.text(sdf['x'].mean(), sdf['y'].mean(), name, ha='center', va='center')

for sand in range(0, 101, 20):
    ax.text(100 - sand, -1, f"{sand:d}", ha='center', va='top')

for clay in range(0, 101, 20):
    ax.text(clay * cos(radians(60)) - 1, clay * cos(radians(30)), f"{clay:d}", ha='right', va='center')

ax.set_xticks([])
ax.set_yticks([])

fig.tight_layout()
plt.show()

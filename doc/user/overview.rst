Overview
========

This package provides a couple of algorithms to convert texture names into their
sand, clay fraction counterpart as defined by the
`USDA <https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcs142p2_054253#soil_texture>`_

.. image:: triangle-texture-soils.png

